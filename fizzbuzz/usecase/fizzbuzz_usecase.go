package usecase

import (
	"fmt"

	"leboncoin/domain"
	"leboncoin/log"
)

type fizzbuzzUsecase struct{}

func NewFizzbuzzUsecase() domain.FizzBuzzUsecase {
	return &fizzbuzzUsecase{}
}

// GenerateFizzBuzz render fuzzybuzz list from parameters
func (s *fizzbuzzUsecase) GenerateFizzBuzz(fizzNumber, buzzNumber, limit int, fizzString, buzzString string) []string {
	log.AllFieldsInfo("[GenerateFizzBuzz] Generate list", map[string]interface{}{
		"fizzNumber": fizzNumber,
		"buzzNumber": buzzNumber,
		"limit":      limit,
		"fizzString": fizzString,
		"buzzString": buzzString,
	})
	var results = make([]string, limit)

	for i := 1; i <= limit; i++ {
		var result string

		if i%fizzNumber == 0 && i%buzzNumber == 0 {
			result = fmt.Sprintf("%s%s", fizzString, buzzString)
		} else if i%fizzNumber == 0 {
			result = fizzString
		} else if i%buzzNumber == 0 {
			result = buzzString
		} else {
			result = fmt.Sprintf("%d", i)
		}

		results[i-1] = result

	}
	return results
}
