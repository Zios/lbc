package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateFizzBuzz(t *testing.T) {
	fizzBuzzUsecase := NewFizzbuzzUsecase()
	t.Run("[Success] Generate basic fizz buzz list", func(t *testing.T) {
		assert.Equal(
			t,
			fizzBuzzUsecase.GenerateFizzBuzz(3, 5, 100, "Fizz", "Buzz"),
			[]string{
				"1", "2", "Fizz", "4", "Buzz",
				"Fizz", "7", "8", "Fizz", "Buzz",
				"11", "Fizz", "13", "14", "FizzBuzz",
				"16", "17", "Fizz", "19", "Buzz",
				"Fizz", "22", "23", "Fizz", "Buzz",
				"26", "Fizz", "28", "29", "FizzBuzz",
				"31", "32", "Fizz", "34", "Buzz",
				"Fizz", "37", "38", "Fizz", "Buzz",
				"41", "Fizz", "43", "44", "FizzBuzz",
				"46", "47", "Fizz", "49", "Buzz",
				"Fizz", "52", "53", "Fizz", "Buzz",
				"56", "Fizz", "58", "59", "FizzBuzz",
				"61", "62", "Fizz", "64", "Buzz",
				"Fizz", "67", "68", "Fizz", "Buzz",
				"71", "Fizz", "73", "74", "FizzBuzz",
				"76", "77", "Fizz", "79", "Buzz",
				"Fizz", "82", "83", "Fizz", "Buzz",
				"86", "Fizz", "88", "89", "FizzBuzz",
				"91", "92", "Fizz", "94", "Buzz",
				"Fizz", "97", "98", "Fizz", "Buzz",
			},
		)
	})
	t.Run("[Success] Generate basic fizz buzz list", func(t *testing.T) {
		assert.Equal(
			t,
			fizzBuzzUsecase.GenerateFizzBuzz(3, 5, 100, "Yo", "lo"),
			[]string{
				"1", "2", "Yo", "4", "lo",
				"Yo", "7", "8", "Yo", "lo",
				"11", "Yo", "13", "14", "Yolo",
				"16", "17", "Yo", "19", "lo",
				"Yo", "22", "23", "Yo", "lo",
				"26", "Yo", "28", "29", "Yolo",
				"31", "32", "Yo", "34", "lo",
				"Yo", "37", "38", "Yo", "lo",
				"41", "Yo", "43", "44", "Yolo",
				"46", "47", "Yo", "49", "lo",
				"Yo", "52", "53", "Yo", "lo",
				"56", "Yo", "58", "59", "Yolo",
				"61", "62", "Yo", "64", "lo",
				"Yo", "67", "68", "Yo", "lo",
				"71", "Yo", "73", "74", "Yolo",
				"76", "77", "Yo", "79", "lo",
				"Yo", "82", "83", "Yo", "lo",
				"86", "Yo", "88", "89", "Yolo",
				"91", "92", "Yo", "94", "lo",
				"Yo", "97", "98", "Yo", "lo",
			},
		)
	})

	t.Run("[Success] No list", func(t *testing.T) {
		assert.Equal(
			t,
			fizzBuzzUsecase.GenerateFizzBuzz(3, 5, 0, "Fizz", "Buzz"),
			[]string{},
		)
	})
}
