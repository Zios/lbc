package http

import (
	"leboncoin/domain"
	"leboncoin/domain/mocks"

	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestFizzBuzz(t *testing.T) {
	// Disable gin logs
	gin.SetMode(gin.ReleaseMode)
	fizzBuzzList := []string{
		"1",
		"Fizz",
		"Buzz",
		"Fizz",
		"5",
		"FizzBuzz",
		"7",
		"Fizz",
		"Buzz",
		"Fizz",
		"11",
		"FizzBuzz",
		"13",
		"Fizz",
		"Buzz",
	}
	mockFizzBuzzUsecase := new(mocks.FizzBuzzUsecase)
	mockStatisticUsecase := new(mocks.StatisticUsecase)

	t.Run("[Success] Everything work fine (trim parameters)", func(t *testing.T) {
		//Mock services called
		mockFizzBuzzUsecase.On(
			"GenerateFizzBuzz",
			2,
			3,
			15,
			"Fizz",
			"Buzz",
		).Return(fizzBuzzList).Once()
		mockStatisticUsecase.On(
			"UpdateStatistic",
			mock.Anything,
			mock.Anything,
			mock.Anything,
			mock.Anything,
			mock.Anything,
		).Return(nil, nil).Once()

		router := gin.New()
		rec := httptest.NewRecorder()
		parameters := url.Values{}

		NewFizzBuzzHttp(router, mockFizzBuzzUsecase, mockStatisticUsecase)
		parameters.Add("limit", "   15   ")
		parameters.Add("fizzNumber", "2     ")
		parameters.Add("buzzNumber", "   3")
		parameters.Add("fizzString", "  Fizz")
		parameters.Add("buzzString", "Buzz    ")

		req, err := http.NewRequest("GET", fmt.Sprintf("/fizzbuzz?%s", parameters.Encode()), nil)

		assert.NoError(t, err)
		router.ServeHTTP(rec, req)

		stringifyResponse, err := json.Marshal(fizzBuzzList)
		assert.NoError(t, err)

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(
			t,
			fmt.Sprintf("{\"FizzBuzz\":%s}", string(stringifyResponse)),
			rec.Body.String(),
		)

		mockFizzBuzzUsecase.AssertExpectations(t)
	})

	t.Run("[Error] Missing parameters", func(t *testing.T) {
		router := gin.New()
		rec := httptest.NewRecorder()
		parameters := url.Values{}

		NewFizzBuzzHttp(router, mockFizzBuzzUsecase, mockStatisticUsecase)
		parameters.Add("limit", "15")
		parameters.Add("fizzNumber", "2")
		parameters.Add("buzzNumber", "3")

		req, err := http.NewRequest("GET", fmt.Sprintf("/fizzbuzz?%s", parameters.Encode()), nil)

		assert.NoError(t, err)
		router.ServeHTTP(rec, req)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(
			t,
			fmt.Sprintf("{\"errors\":{\"buzzString\":\"%s\",\"fizzString\":\"%s\"}}", domain.ErrMissingValue, domain.ErrMissingValue),
			rec.Body.String(),
		)

		mockFizzBuzzUsecase.AssertExpectations(t)
	})

	t.Run("[Error] Can't parse integer", func(t *testing.T) {
		router := gin.New()
		rec := httptest.NewRecorder()
		parameters := url.Values{}

		NewFizzBuzzHttp(router, mockFizzBuzzUsecase, mockStatisticUsecase)
		parameters.Add("limit", "yolo")
		parameters.Add("fizzNumber", "2")
		parameters.Add("buzzNumber", "3")
		parameters.Add("fizzString", "Fizz")
		parameters.Add("buzzString", "Buzz")

		req, err := http.NewRequest("GET", fmt.Sprintf("/fizzbuzz?%s", parameters.Encode()), nil)

		assert.NoError(t, err)
		router.ServeHTTP(rec, req)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(
			t,
			fmt.Sprintf("{\"errors\":{\"limit\":\"%s\"}}", domain.ErrConvertStringToInt),
			rec.Body.String(),
		)

		mockFizzBuzzUsecase.AssertExpectations(t)
	})
}
