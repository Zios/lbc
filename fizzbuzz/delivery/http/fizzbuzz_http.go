package http

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"

	"leboncoin/domain"
	"leboncoin/log"
)

type fizzBuzzHTTP struct {
	fizzBuzzUsecase   domain.FizzBuzzUsecase
	statisticsUsecase domain.StatisticUsecase
}

// NewFizzBuzzHttp will initialize http server in order to serve FizzBuzz functions
func NewFizzBuzzHttp(e *gin.Engine, fizzbuzz domain.FizzBuzzUsecase, stats domain.StatisticUsecase) {
	handler := &fizzBuzzHTTP{
		fizzBuzzUsecase:   fizzbuzz,
		statisticsUsecase: stats,
	}

	e.GET("/fizzbuzz", handler.FizzBuzz)
}

// FizzBuzz generate a fuzzy buzz like list.
// Accepted parameters are :
// - fizzNumber: number and multiple of this number will be replaced by your `fizzString`
// - buzzNumber: number and multiple of this number will be replaced by your `buzzString`
// - fizzString: String use instead of fizzNumber and its multiple
// - buzzString: String use instead of buzzNumber and its multiple
// - limit: number of element in your list
func (s *fizzBuzzHTTP) FizzBuzz(c *gin.Context) {
	log.Infoln("[FizzBuzzHttp] New FizzBuzz request")

	var errors = make(map[string]interface{})

	// Get parameters
	fizzNumberStr := strings.Trim(c.Query("fizzNumber"), " ")
	buzzNumberStr := strings.Trim(c.Query("buzzNumber"), " ")
	limitStr := strings.Trim(c.Query("limit"), " ")
	fizzString := strings.Trim(c.Query("fizzString"), " ")
	buzzString := strings.Trim(c.Query("buzzString"), " ")

	// Convert numbers to int
	fizzNumber, errorFizzNumber := strconv.Atoi(fizzNumberStr)
	buzzNumber, errorBuzzNumber := strconv.Atoi(buzzNumberStr)
	limit, errorLimit := strconv.Atoi(limitStr)

	if fizzString == "" {
		errors["fizzString"] = domain.ErrMissingValue.Error()
	}

	if buzzString == "" {
		errors["buzzString"] = domain.ErrMissingValue.Error()
	}

	if errorFizzNumber != nil {
		log.Warn("[FizzBuzzHttp] Can't parse FizzNumber", "error", errorFizzNumber.Error())
		errors["fizzNumber"] = domain.ErrConvertStringToInt.Error()
	}

	if errorBuzzNumber != nil {
		log.Warn("[FizzBuzzHttp] Can't parse BuzzNumber", "error", errorBuzzNumber.Error())
		errors["buzzNumber"] = domain.ErrConvertStringToInt.Error()
	}

	if errorLimit != nil {
		log.Warn("[FizzBuzzHttp] Can't parse Limit", "error", errorLimit.Error())
		errors["limit"] = domain.ErrConvertStringToInt.Error()
	}

	// All errors find are rendered at once
	if len(errors) != 0 {
		log.AllFieldsError("[FizzBuzzHttp] Input error", errors)
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errors,
		})
		return
	}

	listFizzBuzz := s.fizzBuzzUsecase.GenerateFizzBuzz(fizzNumber, buzzNumber, limit, fizzString, buzzString)
	_, err := s.statisticsUsecase.UpdateStatistic(fizzNumber, buzzNumber, limit, fizzString, buzzString)

	// Render the list even when statistic is not working
	if err != nil {
		log.Error("[FizzBuzzHttp] Can't insert statistic", "error", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"errors":   domain.ErrRedis,
			"FizzBuzz": listFizzBuzz,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"FizzBuzz": listFizzBuzz,
	})
	return
}
