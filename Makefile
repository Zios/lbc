mocks:
	mockery --all --output=domain/mocks

test:
	go test ./...

docker:
	docker-compose up -d --build --force-recreate

run:
	air
