package log

import (
	"github.com/sirupsen/logrus"
)

var (
	Logger Log
)

func init() {
	Logger = NewLogger()
}

type Log struct {
	*logrus.Entry
}

// NewLogger initializes the standard logger
// NOTE: In a real project, I like to add "github.com/kz/discordrus" hook
// in order to trigger discord event when the error log is triggered
func NewLogger() Log {
	var baseLogger = logrus.NewEntry(logrus.New())

	baseLogger.Logger.Formatter = &logrus.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	}

	return Log{baseLogger}
}

func AllFieldsInfo(message string, infos map[string]interface{}) {
	Logger.WithFields(logrus.Fields(infos)).Info(message)
}

func AllFieldsWarn(message string, infos map[string]interface{}) {
	Logger.WithFields(logrus.Fields(infos)).Warn(message)
}

func AllFieldsError(message string, infos map[string]interface{}) {
	Logger.WithFields(logrus.Fields(infos)).Error(message)
}

func Info(message, key string, value interface{}) {
	Logger.WithField(key, value).Info(message)
}

func Warn(message, key string, value interface{}) {
	Logger.WithField(key, value).Warn(message)
}

func Error(message, key string, value interface{}) {
	Logger.WithField(key, value).Error(message)
}

func Infoln(args ...interface{}) {
	Logger.Infoln(args...)
}

func Errorln(args ...interface{}) {
	Logger.Errorln(args...)
}

func Warnf(message string, args ...interface{}) {
	Logger.Warnf(message, args...)
}

func Fatal(args ...interface{}) {
	Logger.Fatalln(args...)
}
