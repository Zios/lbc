# LeBonCoin
## Contents 

- [LeBonCoin](#leboncoin)
  - [Commands](#commands)
  - [Architecture](#architecture)
  - [Choices](#choices)
    - [Database](#database)

## Commands
Depending of the command, you may need to install some tools
 - `make test` run all project test. 
 - `make run` run your app in hot reload mode. You need to install [Air](https://github.com/cosmtrek/air).
 - `make mocks` generate mock interfaces. You need to install [Mockery](https://github.com/vektra/mockery).
 - `make docker` run docker. You need to install `docker-compose` and `docker`.

## Architecture: 

There is 4 main layer: 

- **Domain** : Contracts and constants needed in the project are stored here.
- **Delivery** : Outside interface. Both statistic and fuzzbuzz feature are using `http`
- **Repository**: Inside interface. Here we only use a `redis databases`. 
- **Usecase** : Business logic should happen here. 

I made a graphic to understand the logic : 

![architecture](https://gitlab.com/Zios/lbc/-/raw/master/design-system.png)

## Choices :
### Database 

The main choices I had to make was the database I was going to use in order to keep track of all requests made to the server. Thanks to the architecture, this choice is not a critical one because you can easily change the database. 

I thought any SQL database was to heavy for this simple feature. Redis is light, fast (my request take ~1ms on my computer) and for feature like having statistic on your services, redis seems to be a good choice. 


## Documentation API :

The server is listenning on `8080`.

 - `GET` on `/fizzbuzz`: Generate a fuzzybuzz list.
 __Parameters__
    - `limit`: number *(ex: 10)*
    - `fizzNumber`: number *(ex: 10)*
    - `buzzNumber`: number *(ex: 10)*
    - `fizzString`: string *(ex: buzz)*
    - `buzzString`: string *(ex: fizz)*

 - `GET` on `/statistics`: Get statistics about what the most frequent request has been. 


