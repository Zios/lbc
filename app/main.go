package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"

	fbHttp "leboncoin/fizzbuzz/delivery/http"
	fbUsecase "leboncoin/fizzbuzz/usecase"
	"leboncoin/log"
	statHttp "leboncoin/statistics/delivery/http"
	statRepository "leboncoin/statistics/repository"
	statUsecase "leboncoin/statistics/usecase"
)

var RedisAddress string

func init() {
	RedisAddress = os.Getenv("REDIS_ADDRESS")
}

// Entry point of the application. We connect everything here, repository, usecase, delivery.
func main() {
	log.Infoln("[App][Main] Start leboncoin application")

	// Configure Gin
	router := gin.New()
	router.Use(gin.Recovery())

	// Configure Redis
	c, err := redis.Dial("tcp", RedisAddress)

	if err != nil {
		log.Fatal("Can't connect to redis : ", err.Error())
		return
	}

	defer c.Close()

	statisticRepository := statRepository.NewStatisticRepository(c)

	statisticUsecase := statUsecase.NewFizzbuzzUsecase(statisticRepository)
	fizzBuzzUsecase := fbUsecase.NewFizzbuzzUsecase()

	fbHttp.NewFizzBuzzHttp(router, fizzBuzzUsecase, statisticUsecase)
	statHttp.NewStatisticHttp(router, statisticUsecase)

	router.Run(":8080")
}
