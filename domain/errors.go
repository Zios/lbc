package domain

import "errors"

var (
	ErrMissingValue       = errors.New("value can't be empty")
	ErrConvertStringToInt = errors.New("can't convert string to int")
	ErrRedis              = errors.New("unexpected error while using redis")
	ErrArrayWrongSize     = errors.New("wrong array size")
)
