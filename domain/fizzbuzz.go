package domain

type FizzBuzzUsecase interface {
	GenerateFizzBuzz(fizzNumber, buzzNumber, limit int, fizzString, buzzString string) []string
}
