package domain

type Statistic struct {
	NumberOfHits int64
	FizzNumber   int
	BuzzNumber   int
	Limit        int
	FizzString   string
	BuzzString   string
}

type StatisticUsecase interface {
	GetStatistics() ([]Statistic, error)
	UpdateStatistic(fizzNumber, buzzNumber, limit int, fizzString, buzzString string) (*int64, error)
}

type StatisticRepository interface {
	All() (map[string]string, error)
	Update(key string) (*int64, error)
}
