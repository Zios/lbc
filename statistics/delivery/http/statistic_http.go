package http

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"leboncoin/domain"
	"leboncoin/log"
)

type statisticHTTP struct {
	statisticsUsecase domain.StatisticUsecase
}

func NewStatisticHttp(e *gin.Engine, stats domain.StatisticUsecase) {
	handler := &statisticHTTP{
		statisticsUsecase: stats,
	}

	e.GET("/statistics", handler.Statistics)
}

// Statistics render statistics about /fizzbuzz endpoint usage
func (s *statisticHTTP) Statistics(c *gin.Context) {
	log.Infoln("[Statistics] User get statistics")
	stats, err := s.statisticsUsecase.GetStatistics()

	if err != nil {
		log.Error("[FizzBuzzHttp] Can't get statistic", "error", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{
			"error": domain.ErrRedis.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"Statistics": stats,
	})
}
