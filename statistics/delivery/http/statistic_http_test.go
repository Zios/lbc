package http

import (
	"leboncoin/domain"
	"leboncoin/domain/mocks"

	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestStatistics(t *testing.T) {
	gin.SetMode(gin.ReleaseMode)
	mockStatisticUsecase := new(mocks.StatisticUsecase)
	statisticList := []domain.Statistic{
		{
			NumberOfHits: 5,
			FizzNumber:   2,
			BuzzNumber:   3,
			Limit:        100,
			FizzString:   "Fizz",
			BuzzString:   "Buzz",
		},
		{
			NumberOfHits: 1,
			FizzNumber:   2,
			BuzzNumber:   3,
			Limit:        15,
			FizzString:   "Fizz",
			BuzzString:   "Buzz",
		},
	}

	t.Run("[Success] Get statistics", func(t *testing.T) {
		//Mock services called
		mockStatisticUsecase.On("GetStatistics").Return(statisticList, nil).Once()

		router := gin.New()
		rec := httptest.NewRecorder()

		NewStatisticHttp(router, mockStatisticUsecase)

		req, err := http.NewRequest("GET", "/statistics", nil)

		assert.NoError(t, err)
		router.ServeHTTP(rec, req)

		stringifyResponse, err := json.Marshal(statisticList)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(
			t,
			fmt.Sprintf("{\"Statistics\":%s}", string(stringifyResponse)),
			rec.Body.String(),
		)

		mockStatisticUsecase.AssertExpectations(t)
	})

	t.Run("[Error] Unexpected repository error", func(t *testing.T) {
		//Mock services called
		mockStatisticUsecase.On("GetStatistics").Return(nil, domain.ErrRedis).Once()

		router := gin.New()
		rec := httptest.NewRecorder()

		NewStatisticHttp(router, mockStatisticUsecase)

		req, err := http.NewRequest("GET", "/statistics", nil)

		assert.NoError(t, err)
		router.ServeHTTP(rec, req)

		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(
			t,
			fmt.Sprintf("{\"error\":\"%s\"}", domain.ErrRedis.Error()),
			rec.Body.String(),
		)

		mockStatisticUsecase.AssertExpectations(t)
	})
}
