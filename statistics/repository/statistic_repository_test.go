package repository

import (
	"testing"

	"github.com/rafaeljusto/redigomock"
	"github.com/stretchr/testify/assert"
)

func TestUpdate(t *testing.T) {
	key := "3-7-67-WW8=-bG8="

	t.Run("[Success] Increment value", func(t *testing.T) {
		conn := redigomock.NewConn()
		cmd := conn.Command("INCR", key).Expect(int64(42))

		statRepository := NewStatisticRepository(conn)
		result, err := statRepository.Update(key)

		assert.Nil(t, err)
		assert.Equal(t, *result, int64(42))
		assert.Equal(t, conn.Stats(cmd), 1)
	})
}

func TestAll(t *testing.T) {
	t.Run("[Success] Get all values", func(t *testing.T) {
		conn := redigomock.NewConn()

		cmd1 := conn.Command("KEYS", "*").ExpectStringSlice(
			"2-3-15-Rml6eg==-QnV6eg==",
			"3-7-67-WW8=-bG8=",
		)

		cmd2 := conn.Command("MGET", []uint8("2-3-15-Rml6eg==-QnV6eg=="), []uint8("3-7-67-WW8=-bG8=")).ExpectStringSlice("42", "24")

		statRepository := NewStatisticRepository(conn)
		result, err := statRepository.All()

		assert.Nil(t, err)
		assert.Equal(t, result, map[string]string{
			"2-3-15-Rml6eg==-QnV6eg==": "42",
			"3-7-67-WW8=-bG8=":         "24",
		})
		assert.Equal(t, conn.Stats(cmd1), 1)
		assert.Equal(t, conn.Stats(cmd2), 1)

	})
	t.Run("[Success] No keys", func(t *testing.T) {
		conn := redigomock.NewConn()

		cmd := conn.Command("KEYS", "*").Expect([]interface{}{})

		statRepository := NewStatisticRepository(conn)
		result, err := statRepository.All()

		assert.Nil(t, err)
		assert.Equal(t, result, map[string]string{})
		assert.Equal(t, conn.Stats(cmd), 1)

	})
}
