package repository

import (
	"github.com/gomodule/redigo/redis"
	"leboncoin/domain"
)

type statisticRepository struct {
	client redis.Conn
}

func NewStatisticRepository(client redis.Conn) domain.StatisticRepository {
	return &statisticRepository{
		client: client,
	}
}

// All will find all keys and get value. Then return map[keys]value.
func (s *statisticRepository) All() (map[string]string, error) {
	var result = make(map[string]string)

	reply, err := redis.Values(s.client.Do("KEYS", "*"))

	if err != nil {
		return nil, err
	}
	keys, err := redis.Strings(reply, err)

	if err != nil {
		return nil, err
	}

	if len(keys) == 0 {
		return result, nil
	}

	values, err := redis.Strings(s.client.Do("MGET", reply...))

	if err != nil {
		return nil, err
	}

	for i := 0; i < len(keys); i++ {
		result[keys[i]] = values[i]
	}

	return result, nil
}

// Update increment key. If key doesn't exist, value will be init to 1.
func (s *statisticRepository) Update(key string) (*int64, error) {
	hits, err := redis.Int64(s.client.Do("INCR", key))

	if err != nil {
		return nil, err
	}

	return &hits, nil
}
