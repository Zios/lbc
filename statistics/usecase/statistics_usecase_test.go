package usecase

import (
	"leboncoin/domain"
	"leboncoin/domain/mocks"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncodeKey(t *testing.T) {
	t.Run("[Success] Try simple example", func(t *testing.T) {
		assert.Equal(
			t,
			"2-3-15-Rml6eg==-QnV6eg==",
			EncodeKey(2, 3, 15, "Fizz", "Buzz"),
		)
		assert.Equal(
			t,
			"3-7-67-WW8=-bG8=",
			EncodeKey(3, 7, 67, "Yo", "lo"),
		)
	})
}

func TestDecodeKey(t *testing.T) {
	t.Run("[Success] Try simple example", func(t *testing.T) {
		result, err := DecodeKey("2-3-15-Rml6eg==-QnV6eg==")
		assert.Nil(t, err)
		assert.Equal(
			t,
			*result,
			domain.Statistic{
				FizzNumber: 2,
				BuzzNumber: 3,
				Limit:      15,
				FizzString: "Fizz",
				BuzzString: "Buzz",
			},
		)

		result, err = DecodeKey("3-7-67-WW8=-bG8=")
		assert.Nil(t, err)
		assert.Equal(
			t,
			*result,
			domain.Statistic{
				FizzNumber: 3,
				BuzzNumber: 7,
				Limit:      67,
				FizzString: "Yo",
				BuzzString: "lo",
			},
		)
	})

	t.Run("[Error] Key too short", func(t *testing.T) {
		result, err := DecodeKey("2-3-15")
		assert.Nil(t, result)
		assert.Equal(
			t,
			err.Error(),
			domain.ErrArrayWrongSize.Error(),
		)
	})

	t.Run("[Error] Can't convert string", func(t *testing.T) {
		result, err := DecodeKey("3-yolo-67-WW8=-bG8=")
		assert.Nil(t, result)
		assert.Equal(
			t,
			err.Error(),
			"strconv.Atoi: parsing \"yolo\": invalid syntax",
		)
	})
}

func TestGetStatistics(t *testing.T) {
	mockStatisticRepository := new(mocks.StatisticRepository)
	statistics := map[string]string{
		"2-3-15-Rml6eg==-QnV6eg==": "2",
		"3-7-67-WW8=-bG8=":         "10",
	}
	t.Run("[Success] Get all statistics", func(t *testing.T) {
		mockStatisticRepository.On("All").Return(statistics, nil).Once()
		statisticUsecase := NewFizzbuzzUsecase(mockStatisticRepository)
		stats, err := statisticUsecase.GetStatistics()
		assert.Nil(t, err)
		assert.Equal(t,
			[]domain.Statistic{
				{
					FizzNumber:   2,
					BuzzNumber:   3,
					Limit:        15,
					FizzString:   "Fizz",
					BuzzString:   "Buzz",
					NumberOfHits: int64(2),
				},
				{

					FizzNumber:   3,
					BuzzNumber:   7,
					Limit:        67,
					FizzString:   "Yo",
					BuzzString:   "lo",
					NumberOfHits: int64(10),
				},
			},
			stats,
		)

		mockStatisticRepository.AssertExpectations(t)
	})
}
