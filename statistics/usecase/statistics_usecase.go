package usecase

import (
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"

	"leboncoin/domain"
	"leboncoin/log"
)

type statisticUsecase struct {
	statisticRepo domain.StatisticRepository
}

func NewFizzbuzzUsecase(s domain.StatisticRepository) domain.StatisticUsecase {
	return &statisticUsecase{
		statisticRepo: s,
	}
}

// GetStatistics find all keys/values and parse them in order to render statistics
func (s *statisticUsecase) GetStatistics() ([]domain.Statistic, error) {
	keys, err := s.statisticRepo.All()
	if err != nil {
		return nil, err
	}

	var statitics = make([]domain.Statistic, len(keys))
	var index int

	for key, hits := range keys {
		statistic, err := DecodeKey(key)

		if err != nil {
			return nil, err
		}

		numberOfHits, err := strconv.ParseInt(hits, 10, 64)

		if err != nil {
			return nil, err
		}

		statistic.NumberOfHits = numberOfHits

		statitics[index] = *statistic
		index += 1
	}

	return statitics, nil
}

// UpdateStatistic increment by one number of hit for a given key
func (s *statisticUsecase) UpdateStatistic(fizzNumber, buzzNumber, limit int, fizzString, buzzString string) (*int64, error) {
	log.Infoln("[UpdateStatistic] Update statistic")
	return s.statisticRepo.Update(
		EncodeKey(fizzNumber, buzzNumber, limit, fizzString, buzzString),
	)
}

// EncodeKey generate a key from parameters used
// Key format is fizzNumber-buzzNumber-limit-b64(fizzString)-b64(buzzString)
func EncodeKey(fizzNumber, buzzNumber, limit int, fizzString, buzzString string) string {
	// Encode user fizz and buzz word in order to avoid exploit/bugs
	fizzEncoded := base64.StdEncoding.EncodeToString([]byte(fizzString))
	buzzEncoded := base64.StdEncoding.EncodeToString([]byte(buzzString))

	return fmt.Sprintf("%d-%d-%d-%s-%s", fizzNumber, buzzNumber, limit, fizzEncoded, buzzEncoded)
}

// DecodeKey will decode a given string in order to return the statistic
func DecodeKey(key string) (statistict *domain.Statistic, err error) {
	log.Info("[DecodeKey] Try to decode key", "key", key)

	args := strings.Split(key, "-")

	if len(args) != 5 {
		return nil, domain.ErrArrayWrongSize
	}
	fizzNumber, err := strconv.Atoi(args[0])
	if err != nil {
		log.Error("[StatisticUsecase] Can't convert fizzNumber", "error", err.Error())
		return nil, err
	}

	buzzNumber, err := strconv.Atoi(args[1])
	if err != nil {
		log.Error("[StatisticUsecase] Can't convert buzzNumber", "error", err.Error())
		return nil, err
	}

	limit, err := strconv.Atoi(args[2])
	if err != nil {
		log.Error("[StatisticUsecase] Can't convert limit", "error", err.Error())
		return nil, err
	}

	fizzDecoded, err := base64.StdEncoding.DecodeString(args[3])
	if err != nil {
		log.Error("[StatisticUsecase] Can't convert fizzDecoded", "error", err.Error())
		return nil, err
	}

	buzzDecoded, err := base64.StdEncoding.DecodeString(args[4])
	if err != nil {
		log.Error("[StatisticUsecase] Can't convert buzzDecoded", "error", err.Error())
		return nil, err
	}

	return &domain.Statistic{
		FizzNumber: fizzNumber,
		BuzzNumber: buzzNumber,
		Limit:      limit,
		FizzString: string(fizzDecoded),
		BuzzString: string(buzzDecoded),
	}, err
}
